export class Page<T> {
	public totalPages: number;
	public totalElements: number;
	public pageSize: number;
	public currentPage: number;
	public content: T[];
}

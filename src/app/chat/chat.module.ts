import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatComponent } from './components/chat/chat.component';
import { MaterialModule } from '../material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ChatService } from './chat/service/chat.service';
import { HttpClientModule } from '@angular/common/http';
import { ChatRestService } from './chat/service/chat-rest.service';
import { ChatSocketService } from './chat/service/chat-socket.service';
import { MessageComponent } from './components/message/message.component';
import { UserModule } from '../user/user.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { GravatarService } from './chat/service/gravatar.service';

@NgModule({
  imports: [
		CommonModule,
		UserModule,
		MaterialModule,
		HttpClientModule,
		ReactiveFormsModule,
		InfiniteScrollModule,
		FormsModule
  ],
	declarations: [ChatComponent, MessageComponent],
	providers: [ChatService, ChatRestService, ChatSocketService, GravatarService]
})
export class ChatModule { }

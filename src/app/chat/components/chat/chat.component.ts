import { Component, OnInit, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { Message } from '../../chat/model/message';
import { Subscription } from 'rxjs';
import { UserService } from '../../../user/service/user.service';
import { User } from '../../../user/model/user';
import { ChatRestService } from '../../chat/service/chat-rest.service';
import { ChatSocketService } from '../../chat/service/chat-socket.service';
import { GravatarService } from '../../chat/service/gravatar.service';
import { SafeStyle } from '@angular/platform-browser';
import { AuthService } from 'src/app/auth/service/auth.service';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnDestroy {

	public message: string;
	public messages: Message[] = [];
	private componentSubscriptions: Subscription[];
	private currentPage = 0;
	private userStyle: SafeStyle;
	user: User;
	@ViewChild('chatContainer') private chatContainer: ElementRef;



	constructor(
		private readonly chatService: ChatRestService,
		private readonly chatSocketService: ChatSocketService,
		private readonly userService: UserService,
    private readonly authService: AuthService,
    private readonly route: Router) {
		this.componentSubscriptions = [];
	 }

	ngOnInit() {
		const userSubscription = this.userService.me.subscribe(user => this.user = user);
		const socketSub = this.chatSocketService.onMessage().subscribe(message => {
			this.addMessages(message);
			setTimeout(() => {
				this.scrollToBottom();
			}, 0);
		});
		this.nextPage();
		this.componentSubscriptions.push(socketSub, userSubscription);
	}

	private addMessages(...messages: Message[]): void {
		this.messages = this.messages.concat(messages).sort( (left, right) => left.timestamp > right.timestamp ? 1 : -1);
	}

	isMine(message: Message): boolean {
		if (!this.user) return false;
		return message.from.id === this.user.id;
	}

	onSend() {
		if(this.chatSocketService.sendMessage(this.message)) {
			this.message = '';
		}
  }

  onLogout() {
    this.authService.logout();
    this.route.navigate(['login']);
  }

	nextPage() {
		this.chatService.getMessages(new Date(), this.currentPage++).subscribe(messages => {
			this.addMessages(...messages.content);
			if (this.currentPage === 1) {
				setTimeout(() => {
					this.scrollToBottom();
				}, 0);
			}
		});
	}

	ngOnDestroy(): void {
		this.componentSubscriptions.forEach(sub => sub.unsubscribe());
	}

	private scrollToBottom() {
		const container: HTMLElement = this.chatContainer.nativeElement;
		container.scrollTop = container.scrollHeight;
	}
}

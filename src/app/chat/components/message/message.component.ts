import { Component, OnInit, Input } from '@angular/core';
import { Message } from '../../chat/model/message';
import {Md5} from 'ts-md5/dist/md5';
import { join } from 'path';
import { SafeStyle } from '@angular/platform-browser';
import { GravatarService } from '../../chat/service/gravatar.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {

	@Input()
	public message: Message;
	public style: SafeStyle;

  constructor(private readonly gravatarService: GravatarService) { }

  ngOnInit() {
		this.style = this.gravatarService.getStyle(this.message.from);
  }

}

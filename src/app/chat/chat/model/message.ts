import { User } from "../../../user/model/user";

export class Message {
	id: string;
	from: User;
	message: string;
	timestamp: Date;
}

import { Injectable, OnDestroy } from '@angular/core';
import { Observable, BehaviorSubject, Subscription } from 'rxjs';
import { Message } from '../model/message';
import { AuthService } from '../../../auth/service/auth.service';
import { tap, switchMap } from 'rxjs/operators';
import { ChatRestService } from './chat-rest.service';
import { ChatSocketService } from './chat-socket.service';

@Injectable()
export class ChatService implements OnDestroy{

	private messagesSubject: BehaviorSubject<Message[]> = new BehaviorSubject([]);
	serviceSubscription: Subscription;

	constructor(
		private readonly restClient: ChatRestService,
		private readonly socketClient: ChatSocketService,
		private readonly auth: AuthService
	) {
		this.serviceSubscription = this.auth.isLogged.pipe(
			tap(() => this.messagesSubject.next([])),
			switchMap(() => this.restClient.getMessages(new Date(),0)),
			tap(messages => this.messagesSubject.next([...this.messagesSubject.getValue(), ...messages.content])),
			switchMap(() => this.socketClient.onMessage()),
			tap(message => this.messagesSubject.next([...this.messagesSubject.getValue(), message]))
		).subscribe();
	}

	public get messages(): Observable<Message[]> {
		return this.messagesSubject.asObservable();
	}


	public sendMessage(message: string): boolean {
		return this.socketClient.sendMessage(message);
	}

	ngOnDestroy(): void {
		this.serviceSubscription.unsubscribe();
	}

}



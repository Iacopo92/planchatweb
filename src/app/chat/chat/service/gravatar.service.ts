import { Injectable } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { User } from '../../../user/model/user';
import { Md5 } from 'ts-md5';

@Injectable()
export class GravatarService {

	constructor(private readonly sanitizer: DomSanitizer) {}

	public getStyle(user: User): SafeStyle {
		const url = 'https://www.gravatar.com/avatar/' + Md5.hashStr(user.email).toString();
		const style = `background-image: url(${url})`;
		return this.sanitizer.bypassSecurityTrustStyle(style);
	}
}



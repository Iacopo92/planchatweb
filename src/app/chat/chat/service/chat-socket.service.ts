import { Injectable, OnDestroy } from '@angular/core';
import { Subject, Observable, Subscription } from 'rxjs';
import { Message } from '../model/message';
import { AuthService } from '../../../auth/service/auth.service';
import { environment } from 'src/environments/environment';
import { isNullOrUndefined } from 'util';
import * as io from 'socket.io-client';

@Injectable()
export class ChatSocketService implements OnDestroy {

	private socket: SocketIOClient.Socket;
	private incomingMessageSubject: Subject<Message> = new Subject<Message>();
	private userChangeSubscription: Subscription;
	constructor(private readonly aut: AuthService) {
		this.userChangeSubscription = this.aut.isLogged.subscribe(logged => logged ? this.initializeSocket() : this.closeSocket());
	}


	public onMessage(): Observable<Message> {
		return this.incomingMessageSubject.asObservable();
	}

	public sendMessage(text: string): boolean {
		if (isNullOrUndefined(text) || isNullOrUndefined(this.socket)) return false;
		this.socket.emit('add', text);
		return true;
	}

	private initializeSocket(): void {
		this.closeSocket();
    const access = this.aut.getAccessToken();
    const socketEndpoint = environment.socketUrl + '/messages'
		this.socket = isNullOrUndefined(access) ? null : io( socketEndpoint, {
			transports: ['websocket'],
				query: {
					'Authorization': `Bearer ${access}`
				}
			}).connect();
			this.socket.on('message', message => {
        this.incomingMessageSubject.next(message)
      });
	}

	private closeSocket() {
		if (!isNullOrUndefined(this.socket) && this.socket.connected) {
			this.socket.close();
		}
	}
	ngOnDestroy(): void {
		this.userChangeSubscription.unsubscribe();
		this.closeSocket();
	}
}

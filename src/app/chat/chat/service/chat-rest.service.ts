import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Message } from '../model/message';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Page } from '../../../http/model/page';

@Injectable()
export class ChatRestService {
	private messagesSubject: BehaviorSubject<Message[]>;
	private sendNewMessage: (text: string) => void;

	constructor(private readonly http: HttpClient) {

	}


	public getMessages(endDate: Date, page: number): Observable<Page<Message>> {
		return this.http.get<Page<Message>>(`${environment.apiUrl}/api/messages`, {
			params: {endDate: endDate.toJSON(), page: page.toString() }
		});
	}
}



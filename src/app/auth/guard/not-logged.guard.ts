import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../service/auth.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NotLoggedUserGuard implements CanActivate {

	constructor(private router: Router, private readonly authService: AuthService){}

  canActivate(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
			return this.authService.isLogged.pipe(map(logged => {
				if (logged) {
					this.router.navigate([]);
					return false;
				}
				return true;
			}));
	}
}

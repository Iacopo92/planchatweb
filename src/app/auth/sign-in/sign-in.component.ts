import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidationErrors, AbstractControl, ValidatorFn } from '@angular/forms';
import { AuthService } from '../service/auth.service';
import { tap, catchError } from 'rxjs/operators';
import { RestError, RestErrorMessage } from '../../errors/errors';
import { Router } from '@angular/router';

@Component({
	selector: 'app-sign-in',
	templateUrl: './sign-in.component.html',
	styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
	form: FormGroup;

	public get validForm(): boolean {
		return this.form.valid;
	}
	constructor(private readonly authService: AuthService, private readonly router: Router) {}

	ngOnInit() {
		this.form = this.buildForm();
	}

	signin() {
		const {email, username, password} =	this.form.value;
		this.authService.signIn(email, username, password)
		.pipe(
			tap(() => this.router.navigate(['login']))
		)
		.subscribe();
	}

	private buildForm(): FormGroup {
		return new FormGroup({
			email: new FormControl(null, Validators.compose([Validators.required,Validators.email])),
			username: new FormControl(null, Validators.compose([Validators.minLength(8), Validators.required])),
			password: new FormControl(null, Validators.compose([Validators.minLength(8), Validators.required])),
			repassword: new FormControl(null, Validators.compose([Validators.minLength(8), Validators.required, this.samePasswordValidator]))
		});
	}

	private samePasswordValidator(): ValidatorFn {
		return (controller: FormControl) => {
			const p = controller.value;
			const r =	this.form.get('password').value;
			if( p === r ) return null;
			return {
				incorrectPassword : 'invalid'
			};
		}
	}
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../service/auth.service';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit , OnDestroy{

	form: FormGroup;
	loginSubscription: Subscription;
	constructor(private readonly auth: AuthService, private readonly router: Router) {}

	ngOnInit() {

		this.form = this.buildForm();
	}

	public login(): void {
		const {email, password} = this.form.value;
		this.loginSubscription = this.auth.login(email, password)
		.pipe(tap(this.loginAction())).subscribe();
	}

	public goToSignin() {
		this.router.navigate(['signin']);
	}

	private loginAction() {
		return (logged: boolean) => {
			if (logged) {
				this.router.navigate(['']);
			} else {
				this.form.setErrors({badCredentials: true})
			}
		}
	}

	private buildForm(): FormGroup {
		return new FormGroup({
			email: new FormControl('', Validators.compose([Validators.email, Validators.required])),
			password: new FormControl('',Validators.compose([Validators.minLength(8), Validators.required]))
		});
	}

	ngOnDestroy(): void {
		//if(this.loginSubscription)	this.loginSubscription.unsubscribe();
	}
}

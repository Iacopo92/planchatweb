import { Injectable } from '@angular/core';

import {
	HttpInterceptor,
	HttpRequest,
	HttpHandler,
	HttpEvent,
	HttpResponse,
	HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError, of } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError} from 'rxjs/operators';
import { Router } from '@angular/router';
import { isNullOrUndefined } from 'util';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
	constructor(private readonly authService: AuthService, private readonly router: Router) {}
	intercept(
		request: HttpRequest<any>,
		next: HttpHandler
	): Observable<HttpEvent<any>> {
		request = this.setAccessToken(this.authService.getAccessToken(), request);
		return next
			.handle(request)
			.pipe(
				catchError( err => this.catchUnauthorized(err))
			);
	}

	private setAccessToken(accessToken: string, request: HttpRequest<any>): HttpRequest<any> {
		return isNullOrUndefined(accessToken) ? request : request.clone({
			setHeaders: {
				Authorization: `Bearer ${accessToken}`
			}
		});
	}

	private catchUnauthorized(err: any): Observable<HttpEvent<any>>{
		if (err instanceof HttpErrorResponse) {
			if (err.status === 401) {
				this.authService.logout();
				this.router.navigate(['./login']);
			}
		}
		return of(null);
	}

}

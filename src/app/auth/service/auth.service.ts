import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError, BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment.prod';
import { map, tap, catchError, share } from 'rxjs/operators';
import { User } from '../../user/model/user';
import { RestError } from '../../errors/errors';
import { isNullOrUndefined } from 'util';

@Injectable()
export class AuthService {

	private isLoggedSubject: BehaviorSubject<boolean>;

	constructor(private http: HttpClient) {
		const token = localStorage.getItem('token');
		this.isLoggedSubject = new BehaviorSubject(!isNullOrUndefined(token));
	}

	login(email: string, password: string): Observable<boolean> {
		return this.http.post<any>(`${environment.authUrl}/auth/token`, {email, password})
		.pipe(
			tap(response =>  {
				this.setAccessToken(response.token);
			}),
			map(() => true),
			catchError(() => of(false))
		);
	}

	logout(): void {
		this.setAccessToken(null);
	}

	signIn(email: string, username: string, password: string): Observable<User> {
		return this.http.post<User>(`${environment.authUrl}/auth/signin`, {email, username, password})
		.pipe(
			catchError((httpError: HttpErrorResponse) => {
			return throwError(RestError.parse(httpError.error));
		}));
	}

	public get isLogged(): Observable<boolean> {
		return this.isLoggedSubject.asObservable();
	}

	public getAccessToken(): string {
		return localStorage.getItem('token');
	}

	private setAccessToken(accessToken: string): void {
		const nullValue = isNullOrUndefined(accessToken);
		if (nullValue) {
			localStorage.removeItem('token');
		} else {
			localStorage.setItem('token', accessToken);
		}
		this.isLoggedSubject.next(!nullValue);
	}
}

import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/service/auth.service';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment.prod';
import { join } from 'path';
import { User } from '../model/user';

@Injectable()
export class UserService {

	constructor(private readonly auth: AuthService, private readonly http: HttpClient) { }

	public get me(): Observable<User> {
		return this.auth.isLogged
		.pipe(
			switchMap(logged => {
				if (logged) {
					return this.http.get<User>(`${environment.apiUrl}/api/users/me`);
				 }
				 return of(null)
			})
		)
	}
}

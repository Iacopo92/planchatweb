import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserService } from './service/user.service';
import { AuthModule } from '../auth/auth.module';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [
		CommonModule,
		AuthModule,
		BrowserModule,
  ],
	declarations: [],
	providers: [UserService],
})
export class UserModule { }

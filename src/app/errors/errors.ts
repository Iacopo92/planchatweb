import { HttpStatus } from "./http-status";

export class RestErrorMessage {

	public static readonly USER_ALREADY_EXIST = new RestErrorMessage('USER.CREATE.ALREADY-EXIST', 'La mail specificata esiste di già');

	private constructor(public readonly code, public readonly message) {}

	public static get values(): RestErrorMessage[] {
		return [RestErrorMessage.USER_ALREADY_EXIST];
	}
	public static deserialize(code: string) {
		return this.values.find(value => value.code === code);
	}

}

export class RestError {
	statusCode: HttpStatus;
	error: string;
	message: RestErrorMessage;

	static parse(json: any) {
		const error = new RestError();
		error.statusCode = json.statusCode;
		error.message = RestErrorMessage.deserialize(json.message);
		error.error = json.error;
		return error;
	}
}

import { ErrorHandler, Injectable, Injector } from "@angular/core";
import { MatSnackBar } from "@angular/material";
import { RestError } from "./errors";
import { AppComponent } from "../app.component";
import { Router } from "@angular/router";

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
constructor(private readonly snackBar: MatSnackBar) { }
	handleError(error) {

		let message = 'qualcosa è andato storto';
		if (error instanceof RestError) {
			message = error.message.message;
		}
		this.snackBar.open(message,'ok',{duration: 2000, panelClass: 'snackbar-error'})

		throw error;
	}

}

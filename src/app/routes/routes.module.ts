import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from '../app.component';
import { LoggedUserGuard } from '../auth/guard/login.guard';
import { LoginComponent } from '../auth/login/login.component';
import { AuthModule } from '../auth/auth.module';
import { ChatModule } from '../chat/chat.module';
import { ChatComponent } from '../chat/components/chat/chat.component';
import { SignInComponent } from 'src/app/auth/sign-in/sign-in.component';
import { NotLoggedUserGuard } from '../auth/guard/not-logged.guard';

const routes: Routes = [
	{
		path: '',
		component: AppComponent,
		children: [
			{
				path: '',
				canActivate: [LoggedUserGuard],
				pathMatch: 'full',
				children: [
					{
						path: '',
						component: ChatComponent
					}
				]
			},
			{
				path: 'login',
				component: LoginComponent,
				canActivate: [NotLoggedUserGuard]
			},
			{
				path: 'signin',
				component: SignInComponent,
				canActivate: [NotLoggedUserGuard]
			}
		]
	}
];

@NgModule({
	imports: [
		CommonModule,
		RouterModule.forRoot(routes),
		AuthModule,
		ChatModule
	],
	declarations: [],
	exports: [RouterModule]
})
export class RoutesModule {}

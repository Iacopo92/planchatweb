export const environment = {
	production: true,
	authUrl: 'https://planchat-backend.now.sh',
	apiUrl: 'https://planchat-backend.now.sh',
	socketUrl: 'https://planchat-backend.now.sh'
};
